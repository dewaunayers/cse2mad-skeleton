### What is this repository for? ###

TBD

### How do I get set up? ###
- Create an `App password` in Bitbucket:
    - Click your avatar in bitbucket and select `Personal settings`
    - Click `App passwords`
    - Click `Create app password`
    - Grant whichever permissions you would like (at least read and write are needed)
    - Note the password for later use. Store it somewhere secure.
- Clone this Repo
- Open as an existing project in Android Studio
- In Android Studio, 
	- Click VCS in the menu bar
	- Choose 'Enable version control integration...' from the dropdown
	- Select Git and press OK
	- Right click on your project's `TeamKoala` folder
		- select Git then Add from the dropdown
	- Open Terminal in Android Studio and use the following commands:
		- `git remote add origin https://<user>@bitbucket.org/<path>.git`
	    - `git pull https://<user>@bitbucket.org/<path>.git master --allow-unrelated-histories`
	- When prompted for a password, use the `App password` you created earlier on.
	    - It might be more convenient to select "remember me" before clicking okay.


### Contribution guidelines [TBD] ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? [TBD] ###

* Repo owner or admin
* Other community or team contact